import { writable } from 'svelte/store'

export const power = writable<string>(localStorage.getItem('power') || '0')